package com.longping.flutter.pdfview;

import androidx.annotation.NonNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;

public class PDFViewFlutterPlugin implements FlutterPlugin {

    private static final String CHANNEL_NAME = "plugins.flutter.longping.com/pdfview";

    private MethodChannel methodChannel;

    /**
     * Registers a plugin with the v1 embedding api {@code io.flutter.plugin.common}.
     *
     * <p>Calling this will register the plugin with the passed registrar. However plugins initialized
     * this way won't react to changes in activity or context, unlike {@link
     * PDFViewFlutterPlugin}.
     */
    @SuppressWarnings("unused")
    public static void registerWith(PluginRegistry.Registrar registrar) {
        registrar
                .platformViewRegistry()
                .registerViewFactory(
                        CHANNEL_NAME, new PDFViewFactory(registrar.messenger()));

    }

    @Override
    public void onAttachedToEngine(FlutterPluginBinding binding) {
        binding
                .getPlatformViewRegistry()
                .registerViewFactory(
                        CHANNEL_NAME, new PDFViewFactory(binding.getBinaryMessenger()));
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {

    }

}
