#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#
Pod::Spec.new do |s|
  s.name             = 'flutter_lp_pdfview'
  s.version          = '1.0.6'
  s.summary          = 'Flutter plugin that display a pdf using PDFkit.'
  s.description      = <<-DESC
  A Flutter plugin for display pdf from the library as well as from url
  Downloaded by pub (not CocoaPods).
                       DESC
  s.homepage         = 'https://gitlab.com/rogerkuu/flutter_pdfview'
  s.license          = { :type => 'BSD', :file => '../LICENSE' }
  s.author           = { 'lpmas' => 'developer@lpmas.com' }
  s.source           = { :http => 'https://gitlab.com/rogerkuu/flutter_pdfview' }
  s.documentation_url = 'https://gitlab.com/rogerkuu/flutter_pdfview'
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'

  s.ios.deployment_target = '9.0'
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'VALID_ARCHS' => 'armv7 arm64 x86_64' }
end

