#import "PDFViewFlutterPlugin.h"
#import "FlutterPDFView.h"

@implementation LPPDFViewFlutterPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    LPPDFViewFactory* pdfViewFactory = [[LPPDFViewFactory alloc] initWithMessenger:registrar.messenger];
    [registrar registerViewFactory:pdfViewFactory withId:@"plugins.flutter.longping.com/pdfview"];
}
@end
